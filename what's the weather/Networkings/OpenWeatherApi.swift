//
//  OpenWeatherApi.swift
//  what's the weather
//
//  Created by hermann kao on 01/10/2022.
//

import Foundation
import Alamofire

class OpenWeatherApi {
    var baseUrl = "https://api.openweathermap.org/data/3.0"
    var apiKey = "" // An free apiKey from openweather with oneCall subscribtion https://openweathermap.org/full-price

    func getWeatherFromLatLong(lat: Double, long: Double, imperial: Bool = false, completion: @escaping (Weather) -> Void) {
        if(apiKey.isEmpty) {
            fatalError("No OpenWeather ApiKey Provided")
        }
        AF
            .request("\(baseUrl)/onecall?lat=\(lat)&lon=\(long)&exclude=minutely,alerts&appid=\(apiKey)&units=\(imperial ? "imperial" :"metric")&lang=fr")
            .responseDecodable(of: Weather.self) { response in
                guard let weather = response.value else {
                    print("getWeatherFromLatLong Erreur: \(response.error?.localizedDescription ?? "Erreur inconnue")")
                    return
                }
                completion(weather)
            }
    }

    func getWeatherFromLatLong(lat: Double, long: Double, imperial: Bool = false) async -> Weather {
        await withCheckedContinuation { continuation in
            getWeatherFromLatLong(lat: lat, long: long, imperial: imperial) { result in
                continuation.resume(returning: result)
            }
        }
    }
}
